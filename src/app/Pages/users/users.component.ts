import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/Shared/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  public users = this.userService.getUser()


  constructor(public userService: UserService, public router: Router,) {

  }

  ngOnInit(): void {
  }


  public editUser(param: number) {
    this.router.navigateByUrl(`/user/${param}`);
  }

  public goToLogicTest() {
    this.router.navigateByUrl(`/logic`);
  }

}
