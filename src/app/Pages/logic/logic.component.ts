import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common'

@Component({
  selector: 'app-logic',
  templateUrl: './logic.component.html',
  styleUrls: ['./logic.component.css']
})
export class LogicComponent implements OnInit {


  public first_number: any = '';
  public second_number: any = '';

  public final_array: any = [];
  public loading = false;

  constructor(private location: Location,) { }

  ngOnInit(): void {
  }


  backtoList() {
    this.location.back()
  }

  public logicTest() {

    this.final_array = []


    if (this.loading) {
      return
    }

    let reduce = this.first_number - this.second_number

    console.log('Redue', reduce)

    if (reduce <= -9999) {
      return alert('No puede haber una diferencia Mayor o igual a 9999')
    }

    if (this.first_number > this.second_number) {
      return alert('El primer numero no puede ser mayor que el segundo')
    }

    if (this.first_number == '' || this.second_number == '') {
      return alert('No pueden haber campos vacios')
    }

    this.loading = true



    for (let number = parseInt(this.first_number); number <= parseInt(this.second_number); number++) {
      let dividers = []

      for (let aux = 1; aux <= number; aux++) {
        if (number % aux == 0) {
          dividers.push(aux)
        }
      }

      let sum_numbers = dividers.reduce((x, y) => x + (y * y), 0)

      if (Number.isInteger(Math.sqrt(sum_numbers))) {
        this.final_array.push({
          number,
          'value': sum_numbers
        })
      }

    }

    setTimeout(() => {
      this.loading = false
    }, 2000);
  }

}
