import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/Shared/user.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common'

@Component({
  selector: 'app-detail-user',
  templateUrl: './detail-user.component.html',
  styleUrls: ['./detail-user.component.css']
})



export class DetailUserComponent implements OnInit {

  public userSelected: any = {}

  constructor(private location: Location, public userService: UserService, public router: Router, private actRoute: ActivatedRoute) { }

  ngOnInit(): void {
    let postid = this.actRoute.snapshot.params.id;
    this.userSelected = this.userService.getUserId(postid);
  }


  backtoList() {
    this.location.back()
  }

}
