import { Injectable } from '@angular/core';
import { Observable, throwError, Subject } from "rxjs";



@Injectable({
    providedIn: 'root'
})


export class UserService {

    public users = [
        {
            id: 1,
            name: 'Jose Daniel',
            last_name: "Vivas Rosales",
            phone: "+58 412 3925909",
            direction: "Calle Miranda, Caracas 1060, Distrito Capital",
            type_client: "Vendedor",
            contract_id: 3123,
            date_contract: new Date(),
            profile_image: 'https://yt3.ggpht.com/yti/APfAmoHbd3KaBvnfm5VbfxH7D31dHvktTPmI9FDWGyeEPg=s88-c-k-c0x00ffffff-no-rj-mo',
            tasks: [
                'Realizar pedidos a proveedores',
                'Cambiar data estatica',
                'Prueba de tareas a realizar',
                'Test de integracion'
            ]
        },
        {
            id: 2,
            name: 'Padrito Angel',
            last_name: 'Gomez Gutierres',
            phone: '+58 412 3925909',
            direction: 'Avenida Circunvalacion, Caracas 1030, Distrito Capital',
            type_client: "Inversionista",
            contract_id: 5123,
            date_contract: new Date(),
            profile_image: 'https://yt3.ggpht.com/ytc/AKedOLRDO8_FIgoFxp3KIFG-n-TkO8REe5AyD-SXddslAA=s68-c-k-c0x00ffffff-no-rj',
            tasks: [
                'Llamar inversores',
                'Problemas con endpoints',
                'Posible bug en produccion',
                'Conectar base de datos'
            ]
        },
        {
            id: 3,
            name: 'Nacho Ignacio',
            last_name: "Mora Rituales",
            phone: "+58 412 3925909",
            direction: "Transversal 4, Caracas 1060, Distrito Capital",
            type_client: "Vendedor",
            contract_id: 6123,
            date_contract: new Date(),
            profile_image: 'https://yt3.ggpht.com/ytc/AKedOLQKFTQiiCghlQZUUUr_RKeh5eIpXSudK1OnYpuohw=s68-c-k-c0x00ffffff-no-rj',
            tasks: [
                'Realizar pedidos a proveedores',
                'Realizar pedidos a cocacola',
                'Realizar pedidos a polar',
            ]
        },
        {
            id: 4,
            name: 'Ciccio Paul',
            last_name: 'Sanchez Brito',
            phone: '+58 412 3925909',
            direction: 'Calle Mexico, Caracas 1060, Maracay',
            type_client: "Inversionista",
            contract_id: 4771,
            date_contract: new Date(),
            profile_image: 'https://yt3.ggpht.com/ytc/AKedOLRFHBRYas-QEx-FbVWRwFr2WVgGCe9iSBa-Hhj2BQ=s68-c-k-c0x00ffffff-no-rj',
            tasks: [
                'Cambiar toma de decisiones',
                'Reuniones ceo',
                'Reuniones Inversores',
                'Marketing Grilla'
            ]
        },
        {
            id: 5,
            name: 'Pedrito Nacho',
            last_name: "Labrador Alexander",
            phone: "+58 412 3925909",
            direction: "Calle Francisco de Miranda, Caracas 1060, Distrito Capital",
            type_client: "Vendedor",
            contract_id: 6231,
            date_contract: new Date(),
            profile_image: 'https://yt3.ggpht.com/ytc/AKedOLQPNIocGPdjLejwmJjegNM8apISbR-JSnZ3wJC8sQ=s68-c-k-c0x00ffffff-no-rj',
            tasks: [
                'Comprar Equipos de Oficina',
                'Actualizar tabla de empleados',
                'Prueba de empleados',
            ]
        },
        {
            id: 6,
            name: 'Padrito Angel',
            last_name: 'Gomez Gutierres',
            phone: '+58 412 3925909',
            direction: 'Casalta 3, Caracas 1060, Distrito Capital',
            type_client: "Inversionista",
            contract_id: 7432,
            date_contract: new Date(),
            profile_image: 'https://yt3.ggpht.com/uf6b5RzfsMPCUtaVGVLw-OMkQXIY8LbTl5e-KXXkNvOSY9qhYUpN7Z2exxrDmfgVKSP6U-H41g=s88-c-k-c0x00ffffff-no-rj',
            tasks: [
                'Realizar pedidos a proveedores',
                'Cambiar data estatica',
                'Prueba de tareas a realizar',
                'Test de integracion'
            ]
        },
    ]

    public getUser() {
        return this.users
    }


    public getUserId(id: number) {
        const templateIndex = this.users.find((p) => p.id == id)
        return templateIndex;
    }





}