import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './Pages/users/users.component';
import { DetailUserComponent } from './Pages/detail-user/detail-user.component';
import { LogicComponent } from './Pages/logic/logic.component';

const routes: Routes = [
  {
    component: LogicComponent,
    path: 'logic'
  },
  {
    component: UsersComponent,
    path: 'user'
  },
  {
    component: UsersComponent,
    path: ''
  },
  {
    component: DetailUserComponent,
    path: 'user/:id'
  },
  { path: '**', redirectTo: '/' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
